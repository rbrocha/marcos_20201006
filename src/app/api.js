import axios from 'axios';
import qs from 'query-string';
import Base64 from 'Base64';

import {Constants} from '../utils';

const api = axios.create({
  baseURL: Constants.BASE_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});

const apiBasic = axios.create({
  baseURL: Constants.BASE_URL,
  headers: {
    Authorization: `Basic ${Base64.btoa(
      Constants.API_BASIC_USER + ':' + Constants.API_BASIC_PSWD,
    )}`,
  },
});

const oAuthRequest = async (url, data) => {
  const response = await apiBasic.post(url, qs.stringify(data), {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  });

  return response;
};

export {api, oAuthRequest};
