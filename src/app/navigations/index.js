import 'react-native-gesture-handler';
import React from 'react';
import {useSelector} from 'react-redux';

import {NavigationContainer} from '@react-navigation/native';

import AppNavigator from './appNavigator';
import AuthNavigator from './authNavigator';

const RootNavigator = () => {
  const signed = useSelector((state) => state.auth.signed);
  return (
    <NavigationContainer>
      {signed ? <AppNavigator /> : <AuthNavigator />}
    </NavigationContainer>
  );
};

export default RootNavigator;
