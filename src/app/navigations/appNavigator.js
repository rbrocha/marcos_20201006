import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Home from '../../views/home';

const Stack = createStackNavigator();

const AppNavigator = () => {
  return (
    <Stack.Navigator initialRouteName="Home" headerMode="none" header={null}>
      <Stack.Screen name="Home" component={Home} />
    </Stack.Navigator>
  );
};

export default AppNavigator;
